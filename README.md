# Using Solace's SEMPv2 to integrate with CI/CD pipelines using Ansible and Bitbucket

This framework demonstrates how you can use Solace's SEMPv2 management API to perform automated provisioning of Solace configuration. 

There are two ways you can get started:

## Contents

[Ansible](https://www.ansible.com/) is an open source tool for automating tasks such as application deployment, IT service orchestration, cloud provisioning and many more. Ansible is easy to deploy, does not use any agents, and has a simple learning curve. Ansible uses "playbooks" to describe its automation jobs, known as "tasks" and the playbooks are described using YAML.

This repository contains an ansible playbook which uses the Solace SEMPv2 RESTful administration API to create a new messaging environment on an existing Solace message router. Ansible's [URI](http://docs.ansible.com/ansible/latest/uri_module.html) module is used to interact with the SEMPv2 API. 

For a nice introduction to the SEMPv2 Management API , check out this [blog](https://solace.com/blog/products-tech/introducing-semp-v2-solace-message-routers-configuration-reinvented), as well as the [SEMP tutorials home page](http://dev.solace.com/get-started/semp-tutorials/)

## Automated provisioning in a CI environment using Ansible

Using Ansible, you can automatically provision configuration on the Solace router. The Ansible playbook consists of a number of tasks, and each task uses Solace's SEMPv2 Management API to create a new Solace message-vpn on an existing Solace message router along with associated objects:

- Client Profiles
- ACL Profiles
- Client Usernames
- Queue Endpoints

If the objects already exist, this is indicated in the playbook run's output. It is not treated as a failure of the Ansible task, and the playbook execution continues to to the next task. Object properties can be specified in a configuration files.

![CI Flow Diagram](https://github.com/srajgopalan/solace-ci-cd-demo/blob/master/images/CI.jpg "Continuous Integration using Anisble and SEMPv2")

1. __Edit the configuration files__: The configuration for Solace environments to be created is specified in `vars/solace-env.yml`. Edit the configuration objects as necessary. The current version of the sample supports the creation of:

- One or more ACL profiles within the message-vpn
- One or more Client Usernames within the message-vpn
- One or more Queues within the message-vpn, with topic subscriptions on these queues
  
  Deletion of
- One or more ACL profiles within the message-vpn
- One or more Client Usernames within the message-vpn
- One ore more Queues within the messsage-vpn, with topic subscriptions on these queues

__NOTE:__ 

- The current version of this framework does not support the externalization of all the configuration properties for the Solace environment. Only some properties are specified in the configuration files, and more properties can be added along with appropriate changes to your ansible playbook, depending on your environment.
- The sample currently does not have the feature to remove any message-vpns when they are removed from the configuration file.This operation will have to be performed manually.
- The current version of this framework does not support creation of messaging VPN as per the agreement.
- The current version of this framework does not support creation of client-profiles, the default will be used always.

## Running the pipeline:

The pipeline can be run from bitbucket.
Instructions below:
https://barrenjoey.atlassian.net/wiki/spaces/BWTECH/pages/592117817/How+to+create+Solace+objects+from+CI+CD+Pipeline

As indicated previously, the Ansible playbook does not attempt to re-create existing objects - they will simply be logged as "ALREADY_EXISTS"

__NOTE:__ If any configuration is removed, the Ansible playbook does not delete the corresponding configurations from the Solace router

Sample Configuration inputs to be added in solace-evn.yml
--------------------------------------------------------------------------------------
#acl-profiles
acl_profiles:
- aclProfileName: "testaclnew"
  publishTopicException:
  - "test>"
  publishTopicExceptionSyntax: "smf"
  publishTopicDefaultAction: "disallow"
  clientConnectExceptionAddress:
  - "0.0.0.0/24"
  - "1.2.3.4/22"
  clientConnectDefaultAction: "disallow"
  subscribeTopicDefaultAction: "disallow"
  subscribeTopicException:
  - "test>"
  subscribeTopicExceptionSyntax: "smf"
  subscribeShareNameDefaultAction: "disallow"
  subscribeShareNameException:
  - "test"
  subscribeShareNameExceptionSyntax: "smf"

#client-usernames
client_usernames:
- clientUsername: "test_user"
  clientProfileName: "foo_cp"
  aclProfileName: "foo_acl"
  password: "foo_cu"
  enabled: true

#queues
queues:
- queueName:
  subscription_topics:
  - "testtopic/*"
    accessType: "non-exclusive"
    owner: ""
    ingressEnabled: true
    egressEnabled: true
    permission: "consume"
    maxMsgSpoolUsage: 100

#delete_queues
delete_queues:
- queueName:"test_queue"
  accessType: "non-exclusive"
  owner: ""
  ingressEnabled: true
  egressEnabled: true
  permission: "consume"
  maxMsgSpoolUsage: 100

#acl-profiles
delete_acl_profiles:
- aclProfileName: "test_acl"

#delete-client-usernames
delete_client_usernames:
- clientUsername: "test_cu"
---------------------------------------
## Built With

- [Ansible](https://www.ansible.com) - Simple IT Automation, Configuration Management and Orchestration
- [SEMPv2](https://docs.solace.com/SEMP/SEMP-Home.htm) - Solace's RESTful Administration API 
- [BitBucket](https://jenkins.io/) - Open Source Automation Server


## Resources

For more information try these resources:

- The Solace Developer Portal website at: http://dev.solace.com
- Get a better understanding of [Solace technology](http://dev.solace.com/tech/).
- Get your hands dirty with [SEMP Tutorials](http://dev.solace.com/get-started/semp-tutorials/)
- Check out the [Solace blog](http://dev.solace.com/blog/) for other interesting discussions around Solace technology
- Ask the [Solace community.](http://dev.solace.com/community/)
