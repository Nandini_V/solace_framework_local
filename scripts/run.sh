aws configure set aws_access_key_id ${aws_access_key_id}
aws configure set aws_secret_access_key ${aws_secret_access_key}
aws configure set region ap-southeast-2
echo ${aws_secret_name}
"$(python --version)"
"$(aws --version)"
aws configure list --output json
ansible-playbook create-acl-profiles.yaml -i inventory.ini -e env=$env
ansible-playbook create-client-usernames.yaml -i inventory.ini -e env=$env
ansible-playbook create-solace-queue.yaml -i inventory.ini -e env=$env
ansible-playbook create-queue-subscriptions.yaml -i inventory.ini -e env=$env
ansible-playbook create-solace-jndi-queue.yaml -i inventory.ini -e env=$env
ansible-playbook create-solace-jndi-topic.yaml -i inventory.ini -e env=$env
ansible-playbook create-publish-topicException.yaml -i inventory.ini -e env=$env
ansible-playbook create-subscribe-topicException.yaml -i inventory.ini -e env=$env
ansible-playbook create-client-connectException.yaml -i inventory.ini -e env=$env
ansible-playbook create-subscribe-shareNameException.yaml -i inventory.ini -e env=$env
ansible-playbook update-client-usernames.yaml -i inventory.ini -e env=$env
ansible-playbook update-solace-queue.yaml -i inventory.ini -e env=$env
          #- ansible-playbook delete-solace-queue.yaml -i inventory.ini -e env=$env
          #- ansible-playbook delete-acl-profiles.yaml -i inventory.ini -e env=$env
          #- ansible-playbook delete-client-usernames.yaml -i inventory.ini -e env=$env